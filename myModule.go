package myModule

import (
	"fmt"
)

func Version() {
	fmt.Println("Version 1.1.0")
}

func AddFeature() string {
	return "super feature"
}
